# Overview:
This project is to build and test anomaly detection model for time-series data
that are collected from a turtlebot.
The collected datasets are located under `dataset` folder, which will be
automatically downloaded and extracted for you when training or running inference.
There is a README file under `dataset/` folder, please read it if you want to
know more about how data are collected.

After you train the model with `store_result`, the `logfiles` will log during
 train/test process, and also real-time monitoring via *wandb.ai* web service.
 The models will be stored under `trained_model`, while the processed data are
 saved under `results` folder (for further evaluation, e.g., process in Notebook).

**If you find any problems, issues, please open an `issue` with log files, and detailed description. We will fix it as soon as possible with our available resources.
Thank you!**

# Prerequisites:
Install all requirements in `requiremets.txt`.

# User Guide:
## Pytorch:
```
python3 train_pytorch.py --help
usage: train_pytorch.py [-h] [--verbose] [--test] [--dataset DATASET]
                        [--nb_epochs NB_EPOCHS] [--model MODEL] [--plot]
                        [--store_result] [--attention]

optional arguments:
  -h, --help            show this help message and exit
  --verbose
  --test                Only test the trained model (stored in trained_model
                        folder).
  --dataset DATASET     Experiment dataset: turtlebot3. Default is turtlebot3.
  --nb_epochs NB_EPOCHS
                        Number of epochs for training. Default is 2.
  --model MODEL         Model name: lstm_seq2seq, and gru_seq2seq (default).
  --plot                Enable plot graph if running with Xserver.
  --store_result        Enable to store input and reconstructed results of
                        valid and test sets.
  --attention           Enable to attention decoder mode.
```

The recommended configuration is in the `confgi/turtlebot3_config_pytorch_attention.py`.

* Train the model:
```
 CUDA_VISIBLE_DEVICES="1" python3 train_pytorch.py   --model lstm_seq2seq --store_result --nb_epochs 250 --attention
```
If using first GPU, no need to specify `CUDA_VISIBLE_DEVICES="x"`:
```
CUDA_VISIBLE_DEVICES="3" python3 train_pytorch.py --model gru_seq2seq --store_result --nb_epochs 250 --verbose
```

* Test model:

```
CUDA_VISIBLE_DEVICES="1" python3 train_pytorch.py   --model lstm_seq2seq --test --verbose --attention
```

## Tensorflow:
**WARNING: This implementation is not well maintained.**

```
python3 train_tf.py --help
usage: train_tf.py [-h] [--verbose] [--test] [--test_with_gpu]
                   [--dataset DATASET] [--nb_epochs NB_EPOCHS] [--model MODEL]
                   [--plot] [--store_result]

optional arguments:
  -h, --help            show this help message and exit
  --verbose
  --test
  --test_with_gpu       Use GPU device (if available) to load and test the
                        trained model.
  --dataset DATASET     Experiment dataset: turtlebot3. Default is turtlebot3.
  --nb_epochs NB_EPOCHS
                        Number of epochs for training. Default is 2.
  --model MODEL         Model name: lstm_seq2seq (default), and gru_seq2seq.
  --plot                Enable plot graph if running with Xserver.
  --store_result        Enable to store input and reconstructed results of
                        valid and test sets.
```

* Train the model:
```
python3 train_tf.py --model lstm_seq2seq --verbose --nb_epochs 100
```

* Test the trained model:
```
python3 train_tf.py --model lstm_seq2seq --verbose --test
```
