config={
    #"data":
    'robot':'turtlebot3',
    'mode':'normal', #either normal or attack
    'scenario':'pentagon', # either pentagon, or triangle, or zigzag, or house
    'look_back':240,
    'look_ahead_only':False,
    'look_ahead':0, # half second look ahead
    'step_train':1,
    'step_test':5,
    'feature_in':['pose_pose_position_x','pose_pose_position_y'
        #,'pose_pose_orientation_z','pose_pose_orientation_w'
        #,'twist_twist_linear_x','twist_twist_linear_y','twist_twist_angular_z'
        ],
    'feature_out':['pose_pose_position_x','pose_pose_position_y'
        #,'pose_pose_orientation_z','pose_pose_orientation_w'
        #,'twist_twist_linear_x','twist_twist_linear_y','twist_twist_angular_z'
        ],
    #scale: either normalize to [0,1] or standardize to zero mean and unit variance
    'scale':'normalize',
    'downsampling_subset':4, # get mean of k consecutive samples.
    'inversed':False,
    'synthesized_test':False,
    'filter_only_attack':True, #only get windows that contain confident attack
    'synthesized_prob':0.005,
    'synthesized_type':'burst', # either spike or burst
    'synthesized_burst_width':10, # 10 sample width
    #--------------------------------------------------------------

    #"model":
    'encoder_layers':[384, 384],
    #'encoder_layers':[300, 100],
    #'encoder_layers':[300, 200, 100],
    'decoder_layers':[384, 384],
    #'decoder_layers':[300, 100],
    #'decoder_layers':[300, 200, 100],
    'bidirection':True,
    'combine_encoder_states':'concat', #'concat', # either concat or sum
    #--------------------------------------------------------------

    #"training":
    'batch_size':512,
    'droprate':0.10,
    'loss':'mean_absolute_error',#mean_square_error, mean_absolute_error is faster to converge (with early stopping)
    'optimizer':'Adam',
    'learning_rate':0.0005,
    'early_stop_patience':20,
    'use_gpu':True,
    #--------------------------------------------------------------

    # inference
    'batch_size_inference':256,
    #--------------------------------------------------------------
}
