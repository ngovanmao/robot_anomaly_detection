config={
    #"data":
    'robot':'turtlebot3',
    'mode':'normal', # either normal or attack
    'look_back':240,
    'look_ahead_only':False,
    'look_ahead':0,
    'step_train':1,
    'step_test':5,
    'feature_in':['pose_pose_position_x','pose_pose_position_y'
        #,'pose_pose_orientation_z','pose_pose_orientation_w'
        #,'twist_twist_linear_x','twist_twist_linear_y','twist_twist_angular_z'
        ],
    'feature_out':['pose_pose_position_x','pose_pose_position_y'
        #,'pose_pose_orientation_z','pose_pose_orientation_w'
        #,'twist_twist_linear_x','twist_twist_linear_y','twist_twist_angular_z'
        ],
    'downsampling_subset':4, # get mean of k consecutive samples.
    #scale: either normalize to [0,1] or standardize to zero mean and unit variance
    'scale':'normalize',
    'inversed':False,
    'synthesized_test':False, #True if synthesizing, False--use attack data
    'filter_only_attack':False,#True, #only get windows that contain confident attack
    'synthesized_prob':0.05, # 0.005
    'synthesized_type':'burst', # either spike or burst
    'synthesized_burst_width':10, # 10 sample width
    #--------------------------------------------------------------

    #"model":
    'hidden_size':256, #384,# 1024
    'num_layers':1,
    'droprate':0.10, #only apply when num_layers>1
    'bidirection_encoder':True,
    'bidirection_decoder':False,
    'combine_encoder_states':'concat', #'concat', # either concat or sum
    #--------------------------------------------------------------

    #"training":
    'batch_size':512, #256, #64,
    'loss':'mean_absolute_error',#mean_square_error, mean_absolute_error is faster to converge (with early stopping)
    'optimizer':'Adam', #'RMSprop', 'Adam'--the best
    'learning_rate':0.0005,
    'teacher_forcing_ratio':0.75,
    'use_gpu':True,
    'validation':True,
    'cross_validation':True,
    'early_stopping':True,
    'early_stop_patience':20,
    'freq_val':2, # frequency of validation
    #--------------------------------------------------------------

    # inference
    'batch_size_inference':256,
    #--------------------------------------------------------------
}
