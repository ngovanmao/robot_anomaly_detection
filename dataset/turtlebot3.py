import pandas as pd
import time as time
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
import logging
import random
import torch
import os
import requests
import zipfile

def download_dataset():
    if not os.path.exists('dataset/turtlebot3/'):
        url = "https://sutdapac-my.sharepoint.com/:u:/g/personal/vanmao_ngo_sutd_edu_sg/EX06yKNOShZAoJPupI1s4WwBd9YZba9p9smDWkrHCwa1yQ?e=13gvEV&download=1"
        r = requests.get(url, allow_redirects=True)
        open('dataset/turtlebot3_v01_2020Oct15.zip', 'wb').write(r.content)
        with zipfile.ZipFile('dataset/turtlebot3_v01_2020Oct15.zip', 'r') as f:
            f.extractall('dataset/')

def batch_data(data, batch_size, shuffle=False):
    if shuffle:
        np.random.seed(100)
        np.random.shuffle(data)

    batch_data = list()
    for i in range(0, len(data), batch_size):
        if i + batch_size < len(data):
            b_ = data[i: i+batch_size]
        else:
            # Filling the last batch by repeating itself in the round-robin.
            b_temp = data[i:]
            b_ = b_temp
            j = 0
            while len(b_) < batch_size:
                b_ = np.concatenate((b_, b_temp[j:j+1]), axis=0)
                j +=1
                if j == len(b_temp):
                    j = 0
        #b_ = torch.from_numpy(np.asarray(b_)).float()
        batch_data.append(b_)
    return batch_data

def sampling(data, subset=4, method='mean'):
    """
    data: shape=(num_samples, num_feature)
    output: shape=(ceil(num_samples/subset), num_feature)
    """
    assert subset > 0, 'wrong sampling interval, please choose positive number'
    output = []
    i = 0
    while i < len(data):
        w = data[i:i+subset]
        if method == 'mean':
            w = np.mean(w,axis=0)
        output.append(w)
        i += subset
    return np.array(output)

def normalize(data):
    scaler = MinMaxScaler()
    data = scaler.fit_transform(data)
    return data, scaler


# Apply only for training data,
# then use returned scaler object to scale test data
def standardize(data):
    scaler = StandardScaler()
    logging.debug("shape data={}".format(data.shape))
    data = scaler.fit_transform(data)
    logging.debug("shape transformed data={}".format(data.shape))
    logging.debug("Scaler Mean: {}".format(scaler.mean_))
    logging.debug("Scaler Variance: {}".format(scaler.var_))
    logging.debug("data mean {}, data variance {}".format(
        np.mean(data, axis=0),np.var(data, axis=0)))
    return data, scaler

def prepare_seq2seq_data(input_dataset, output_dataset, window_size, look_ahead,
        subset, step_window, inversed=False, look_ahead_only=False):
    #logging.debug("dataset.shape={}".format(dataset.shape))
    #dataset = np.vstack((dataset, dataset[-(window_size+look_ahead):]))
    #logging.debug("input_dataset={}, output_dataset={}".format(
    #    input_dataset.shape, output_dataset.shape))
    data_input, data_target = [], []
    for i in range(0,len(input_dataset) - window_size - look_ahead+1, step_window):
        input_seq = input_dataset[i:(i + window_size)]
        if look_ahead_only:
            output_seq = output_dataset[(i + window_size):(i + window_size + look_ahead)]
        else:
            output_seq = output_dataset[(i + look_ahead):(i + window_size + look_ahead)]
        if inversed:
            output_seq = output_seq[::-1]
        input_seq = sampling(input_seq, subset)
        output_seq = sampling(output_seq, subset)
        data_input.append(input_seq)
        data_target.append(output_seq)
    data_input = np.array(data_input)
    data_target = np.array(data_target)
    logging.debug("data_input.shape={}, data_target.shape={}".format(
        data_input.shape, data_target.shape))
    return data_input, data_target

def get_normal_raw_data(cfg):
    # Load data from csv
    download_dataset()
    data_dir = "dataset/{}/normal_{}".format(cfg['robot'], cfg['scenario'])
    logging.info("get normal raw data at {}".format(data_dir))
    try:
        start = time.time()
        train_input_set = np.load('{}/train_input_set.npy'.format(data_dir))
        train_output_set = np.load('{}/train_output_set.npy'.format(data_dir))
        test_input_set = np.load('{}/test_input_set.npy'.format(data_dir))
        test_output_set = np.load('{}/test_output_set.npy'.format(data_dir))
        logging.info("load dataset from npy files, take={}".format(
            time.time() - start))
    except IOError:
        start = time.time()
        df = pd.read_csv('{}/odometry.csv'.format(data_dir))
        logging.info("load dataset from csv files, take={}".format(
            time.time()-start))
        in_dataset = df[cfg['feature_in']]
        out_dataset = df[cfg['feature_out']]
        logging.info("in_dataset={}, out_dataset={}".format(in_dataset.shape,
            out_dataset.shape))
        len_data =len(in_dataset) # two datasets have the same length.
        if cfg['synthesized_test']:
            ratio_train_test = 0.7
        else:
            ratio_train_test = 0.85
        train_input_set = in_dataset[:int(ratio_train_test*len_data)]
        train_output_set = out_dataset[:int(ratio_train_test*len_data)]
        test_input_set = in_dataset[int(ratio_train_test*len_data):]
        test_output_set = out_dataset[int(ratio_train_test*len_data):]
        """
        np.save('{}/train_input_set.npy'.format(data_dir), train_input_set)
        np.save('{}/train_output_set.npy'.format(data_dir), train_output_set)
        np.save('{}/test_input_set.npy'.format(data_dir), test_input_set)
        np.save('{}/test_output_set.npy'.format(data_dir), test_output_set)
        logging.info("dumped csv files to npy files.")
        """

    logging.info("train_input_set={}, test_input_set={}".format(
        train_input_set.shape, test_input_set.shape))
    #logging.info("train_output_set={}, test_output_data={}".format(
    #    train_output_set.shape, test_output_set.shape))
    return train_input_set, test_input_set, train_output_set, test_output_set


def get_attack_raw_data(cfg):
    data_dir = "dataset/{}/attack_{}".format(cfg['robot'], cfg['scenario'])
    logging.info("get attack raw data from {}".format(data_dir))
    start = time.time()
    df = pd.read_csv('{}/odometry.csv'.format(data_dir))
    logging.info("load dataset from csv files, take={}".format(
        time.time()-start))
    # Load hack_cmd to generate label data.
    hack_df = pd.read_csv('{}/hack_cmd.csv'.format(data_dir))
    def anotation(row):
        if (row['linear_x']==0 and row['linear_y']==0 and row['linear_z']==0
            and row['angular_x']==0 and row['angular_y']==0 and row['angular_z']==0):
            return 0
        else:
            return 1

    hack_df['label'] = hack_df.apply(anotation, axis=1)
    min_sec = hack_df['stamp_secs'].min()
    max_sec = hack_df['stamp_secs'].max()
    labels = {}
    for i in range(min_sec, max_sec + 1):
        temp = hack_df.loc[hack_df['stamp_secs']==i]['label'].values
        labels[i] = 1 if sum(temp)/len(temp) >=0.5 else 0

    def map_label(row, labels):
        h_sec = int(row['h_stamp_secs'])
        return labels[h_sec] if h_sec in labels else 0

    df['label'] = df.apply(map_label, args=(labels,), axis=1)
    in_dataset = df[cfg['feature_in']]
    out_dataset = df[cfg['feature_out']]
    label = df['label'].values
    logging.info("in_dataset={}, out_dataset={}, label={}".format(
        in_dataset.shape, out_dataset.shape, label.shape))

    return in_dataset, out_dataset, label

def gen_ano_spike_indices(length, prob):
    np.random.seed(0)
    rand_prob = np.random.rand(length)
    anomalous_labels = (rand_prob < prob)*1.
    anomalous_indices = np.where(rand_prob < prob)
    return anomalous_indices[0], anomalous_labels

def gen_ano_burst_indices(length, prob, burst_width):
    anomalous_labels = np.zeros(length)
    anomalous_indices = []
    i=0
    np.random.seed(0)
    rand_prob = np.random.rand(length)
    while i < length:
        if rand_prob[i] < prob:
            for j in range(i, i+burst_width):
                if j >= length:
                    break
                else:
                    anomalous_indices.append(j)
                    anomalous_labels[j] = 1.
            i += burst_width
        else:
            i += 1

    return np.array(anomalous_indices), anomalous_labels

def get_data(cfg):
    data_dir = "dataset/{}/cache/{}".format(cfg['robot'], cfg['scenario'])
    try:
        """ Example format: /dataset/kobuki/pentagon_trainIn_LB240_LA0_DS4_stepTr1_
        nonInversed_nonLAO.npy
        train_input, LB (look_back), LA (look_ahead), DS (downsampling_subset),
        stepTr (step_train), nonInversed (non Inversed), nonLAO ()
        """
        start = time.time()
        train_input = np.load('{}_trainIn_LB{}_LA{}_DS{}_step{}_{}Inversed_{}LAO.npy'.
            format(data_dir, cfg['look_back'], cfg['look_ahead'],
                cfg['downsampling_subset'], cfg['step_train'],
                '' if cfg['inversed'] else 'non',
                '' if cfg['look_ahead_only'] else 'non'), allow_pickle=True)
        train_target = np.load('{}_trainTarget_LB{}_LA{}_DS{}_step{}_{}Inversed_{}LAO.npy'.
            format(data_dir, cfg['look_back'], cfg['look_ahead'],
                cfg['downsampling_subset'], cfg['step_train'],
                '' if cfg['inversed'] else 'non',
                '' if cfg['look_ahead_only'] else 'non'), allow_pickle=True)
        input_scaler = np.load('{}_scalerIn_LB{}_LA{}_DS{}_step{}_{}Inversed_{}LAO.npy'.
            format(data_dir, cfg['look_back'], cfg['look_ahead'],
                cfg['downsampling_subset'], cfg['step_train'],
                '' if cfg['inversed'] else 'non',
                '' if cfg['look_ahead_only'] else 'non'), allow_pickle=True)
        output_scaler = np.load('{}_scalerOut_LB{}_LA{}_DS{}_step{}_{}Inversed_{}LAO.npy'.
            format(data_dir, cfg['look_back'], cfg['look_ahead'],
                cfg['downsampling_subset'], cfg['step_train'],
                '' if cfg['inversed'] else 'non',
                '' if cfg['look_ahead_only'] else 'non'), allow_pickle=True)
        test_input = np.load('{}_testIn_LB{}_LA{}_DS{}_step{}_{}Inversed_{}LAO'
            '_{}Synth_{}FOA.npy'.format(data_dir, cfg['look_back'],
                cfg['look_ahead'], cfg['downsampling_subset'], cfg['step_test'],
                '' if cfg['inversed'] else 'non',
                '' if cfg['look_ahead_only'] else 'non',
                '' if cfg['synthesized_test'] else 'non',
                '' if cfg['filter_only_attack'] else 'non'), allow_pickle=True)
        test_target = np.load('{}_testTarget_LB{}_LA{}_DS{}_step{}_{}Inversed_{}LAO'
            '_{}Synth_{}FOA.npy'.format(data_dir, cfg['look_back'],
                cfg['look_ahead'], cfg['downsampling_subset'], cfg['step_test'],
                '' if cfg['inversed'] else 'non',
                '' if cfg['look_ahead_only'] else 'non',
                '' if cfg['synthesized_test'] else 'non',
                '' if cfg['filter_only_attack'] else 'non'), allow_pickle=True)
        test_label = np.load('{}_testLab_LB{}_LA{}_DS{}_step{}_{}Inversed_{}LAO'
            '_{}Synth_{}FOA.npy'.format(data_dir, cfg['look_back'],
                cfg['look_ahead'], cfg['downsampling_subset'], cfg['step_test'],
                '' if cfg['inversed'] else 'non',
                '' if cfg['look_ahead_only'] else 'non',
                '' if cfg['synthesized_test'] else 'non',
                '' if cfg['filter_only_attack'] else 'non'), allow_pickle=True)
        test_label_window = np.load('{}_testLabW_LB{}_LA{}_DS{}_step{}_{}Inversed_{}LAO'
            '_{}Synth_{}FOA.npy'.format(data_dir, cfg['look_back'],
                cfg['look_ahead'], cfg['downsampling_subset'], cfg['step_test'],
                '' if cfg['inversed'] else 'non',
                '' if cfg['look_ahead_only'] else 'non',
                '' if cfg['synthesized_test'] else 'non',
                '' if cfg['filter_only_attack'] else 'non'), allow_pickle=True)

        logging.info("load processed dataset from {}....npy files, take={}".format(
            data_dir, time.time() - start))
        return input_scaler, output_scaler, train_input, train_target,\
               test_input, test_target, test_label, test_label_window
    except IOError:
        if cfg['scenario'] == 'all':
            return get_data_all_without_cache(cfg)
        else:
            return get_data_without_cache(cfg)

def get_data_all_without_cache(cfg):
    scenarios = ['pentagon', 'triangle', 'zigzag']
    dir_cache = "dataset/{}/cache".format(cfg['robot'])
    if not os.path.isdir(dir_cache):
        os.makedirs(dir_cache)
    data_dir = "dataset/{}/cache/{}".format(cfg['robot'], cfg['scenario'])
    ori_train_in, ori_test_in, ori_train_out, ori_test_out = None,None,None,None
    ori_test_att_in, ori_test_att_out, ori_test_att_lb = None, None, None
    for scence in scenarios:
        temp_cfg = cfg
        temp_cfg['scenario'] = scence
        raw_train_in, raw_test_in, raw_train_out, raw_test_out =\
            get_normal_raw_data(temp_cfg)
        raw_test_att_in, raw_test_att_out, raw_test_att_lb =\
            get_attack_raw_data(temp_cfg)
        if ori_train_in is None:
            ori_train_in, ori_test_in, ori_train_out, ori_test_out = \
                raw_train_in, raw_test_in, raw_train_out, raw_test_out
            ori_test_att_in, ori_test_att_out, ori_test_att_lb =\
                raw_test_att_in, raw_test_att_out, raw_test_att_lb
        else:
            ori_train_in = np.concatenate((ori_train_in, raw_train_in), axis=0)
            ori_test_in = np.concatenate((ori_test_in, raw_test_in), axis=0)
            ori_train_out = np.concatenate((ori_train_out, raw_train_out), axis=0)
            ori_test_out = np.concatenate((ori_test_out, raw_test_out), axis=0)

            ori_test_att_in = np.concatenate((ori_test_att_in, raw_test_att_in), axis=0)
            ori_test_att_out = np.concatenate((ori_test_att_out, raw_test_att_out), axis=0)
            ori_test_att_lb = np.concatenate((ori_test_att_lb, raw_test_att_lb), axis=0)
    logging.info("ori_train_in={}, ori_test_in={}, ori_train_out={}, ori_test_out={}"\
        "ori_test_att_in={}, ori_test_test_att_out={}, ori_test_att_lb={}".format(
        ori_train_in.shape, ori_test_in.shape, ori_train_out.shape, ori_test_out.shape,
        ori_test_att_in.shape, ori_test_att_out.shape, ori_test_att_lb.shape))

    return preprocess_data(cfg, data_dir, ori_train_in, ori_test_in, ori_train_out,\
        ori_test_out, ori_test_att_in, ori_test_att_out, ori_test_att_lb)

def get_data_without_cache(cfg):
    data_dir = "dataset/{}/cache/{}".format(cfg['robot'], cfg['scenario'])
    dir_cache = "dataset/{}/cache".format(cfg['robot'])
    if not os.path.isdir(dir_cache):
        os.makedirs(dir_cache)
    origin_train_input, origin_test_input, origin_train_output,\
        origin_test_output = get_normal_raw_data(cfg)
    """ We synthesize the test set on its original data, then generate
    test_label_flat, and compose window-based test_input, test_target,
    and their corresponding test_label_window
    """
    test_label = np.zeros(len(origin_test_input))
    logging.info("ORIGIN: test_label={}, origin_test_input={}".format(
        test_label.shape, origin_test_input.shape))
    if cfg['synthesized_test']:
        if cfg['synthesized_type'] == 'spike':
            ano_indices, test_label = gen_ano_spike_indices(
                len(origin_test_input), cfg['synthesized_prob'])
        else: # cfg['synthesized_type']=='burst'
            ano_indices, test_label = gen_ano_burst_indices(
                len(origin_test_input), cfg['synthesized_prob'],
                cfg['synthesized_burst_width'])
        for fi in range(len(cfg['feature_in'])):
            max_x = origin_train_input.iloc[fi].max()
            min_x = origin_train_input.iloc[fi].min()
            #spike_magnitude = random.uniform(min_x, max_x)
            spike_magnitude = max_x
            #spike_magnitude = 0.5*(max_x+min_x)
            for ai in ano_indices:
                #origin_test_input[ai][fi] += ((-1)**ai)*spike_magnitude
                origin_test_input.iloc[ai][fi] += spike_magnitude

        logging.debug("Number of ANOMALIES={}".format(len(ano_indices)))
        logging.info("SYNTHETIC: test_label={}, origin_test_input={}".format(
            test_label.shape, origin_test_input.shape))

    else:
        origin_test_attack_input, origin_test_attack_output, test_attack_label =\
            get_attack_raw_data(cfg)

    return preprocess_data(cfg, data_dir, origin_train_input, origin_test_input, \
        origin_train_output, origin_test_output, origin_test_attack_input,\
        origin_test_attack_output, test_attack_label)





def preprocess_data(cfg, data_dir, origin_train_input, origin_test_input, \
        origin_train_output, origin_test_output, origin_test_attack_input,\
        origin_test_attack_output, test_attack_label):
    # standardize train set and transform test set
    # We split two scalers for flexibility to have different
    # input and output features
    test_label = np.zeros(len(origin_test_input))
    if cfg['scale'] == 'standardize':
        train_input_data, input_scaler = standardize(origin_train_input)
        train_output_data, output_scaler = standardize(origin_train_output)
    else: # normalize
        train_input_data, input_scaler = normalize(origin_train_input)
        train_output_data, output_scaler = normalize(origin_train_output)

    test_input_data = input_scaler.transform(origin_test_input)
    test_output_data = output_scaler.transform(origin_test_output)

    logging.info("prepare seq2seq for training data")
    train_input, train_target = prepare_seq2seq_data(train_input_data,
        train_output_data, cfg['look_back'], cfg['look_ahead'],
        cfg['downsampling_subset'],
        cfg['step_train'], cfg['inversed'], cfg['look_ahead_only'])

    logging.info("prepare seq2seq for test data")
    test_input, test_target = prepare_seq2seq_data(test_input_data,
        test_output_data, cfg['look_back'], cfg['look_ahead'],
        cfg['downsampling_subset'],
        cfg['step_test'], cfg['inversed'], cfg['look_ahead_only'])

    logging.info("prepare seq2seq for test label")
    _, test_label_window = prepare_seq2seq_data(test_label,
        test_label, cfg['look_back'], cfg['look_ahead'],
        cfg['downsampling_subset'],
        cfg['step_test'], cfg['inversed'], cfg['look_ahead_only'])

    if not cfg['synthesized_test']:
        test_attack_input_data = input_scaler.transform(origin_test_attack_input)
        test_attack_output_data = output_scaler.transform(origin_test_attack_output)

        logging.info("prepare seq2seq for test_attack data")
        test_attack_input, test_attack_target = prepare_seq2seq_data(test_attack_input_data,
            test_attack_output_data, cfg['look_back'], cfg['look_ahead'],
            cfg['downsampling_subset'],
            cfg['step_test'], cfg['inversed'], cfg['look_ahead_only'])

        logging.info("prepare seq2seq for test_attack label")
        _, test_attack_label_window = prepare_seq2seq_data(test_attack_label,
            test_attack_label, cfg['look_back'], cfg['look_ahead'],
            cfg['downsampling_subset'],
            cfg['step_test'], cfg['inversed'], cfg['look_ahead_only'])

        if cfg['filter_only_attack']:
            # Choose only positive attack and combine with test_input
            pos_test_input = []
            pos_test_target = []
            pos_test_label_w = []
            for i, l_w in enumerate(test_attack_label_window):
                if sum(l_w) > 2/3*len(l_w):
                    pos_test_input.append(test_attack_input[i])
                    pos_test_target.append(test_attack_target[i])
                    pos_test_label_w.append(test_attack_label_window[i])
            pos_test_input = np.asarray(pos_test_input)
            pos_test_target = np.asarray(pos_test_target)
            pos_test_label_w = np.asarray(pos_test_label_w)
            test_input = np.concatenate((test_input, pos_test_input), axis=0)
            test_target = np.concatenate((test_target, pos_test_target), axis=0)
            test_label_window = np.concatenate((test_label_window, pos_test_label_w), axis=0)
        else:
            test_input = test_attack_input
            test_target = test_attack_target
            test_label_window = test_attack_label_window

    logging.info("Saving processed data to files {}...".format(data_dir))
    np.save('{}_trainIn_LB{}_LA{}_DS{}_step{}_{}Inversed_{}LAO.npy'.
        format(data_dir, cfg['look_back'], cfg['look_ahead'],
            cfg['downsampling_subset'], cfg['step_train'],
            '' if cfg['inversed'] else 'non',
            '' if cfg['look_ahead_only'] else 'non'), train_input)
    np.save('{}_trainTarget_LB{}_LA{}_DS{}_step{}_{}Inversed_{}LAO.npy'.
        format(data_dir, cfg['look_back'], cfg['look_ahead'],
            cfg['downsampling_subset'], cfg['step_train'],
            '' if cfg['inversed'] else 'non',
            '' if cfg['look_ahead_only'] else 'non'), train_target)
    np.save('{}_scalerIn_LB{}_LA{}_DS{}_step{}_{}Inversed_{}LAO.npy'.
        format(data_dir, cfg['look_back'], cfg['look_ahead'],
            cfg['downsampling_subset'], cfg['step_train'],
            '' if cfg['inversed'] else 'non',
            '' if cfg['look_ahead_only'] else 'non'), input_scaler)
    np.save('{}_scalerOut_LB{}_LA{}_DS{}_step{}_{}Inversed_{}LAO.npy'.
        format(data_dir, cfg['look_back'], cfg['look_ahead'],
            cfg['downsampling_subset'], cfg['step_train'],
            '' if cfg['inversed'] else 'non',
            '' if cfg['look_ahead_only'] else 'non'), output_scaler)
    np.save('{}_testIn_LB{}_LA{}_DS{}_step{}_{}Inversed_{}LAO'
        '_{}Synth_{}FOA.npy'.format(data_dir, cfg['look_back'],
            cfg['look_ahead'], cfg['downsampling_subset'], cfg['step_test'],
            '' if cfg['inversed'] else 'non',
            '' if cfg['look_ahead_only'] else 'non',
            '' if cfg['synthesized_test'] else 'non',
            '' if cfg['filter_only_attack'] else 'non'), test_input)
    np.save('{}_testTarget_LB{}_LA{}_DS{}_step{}_{}Inversed_{}LAO'
        '_{}Synth_{}FOA.npy'.format(data_dir, cfg['look_back'],
            cfg['look_ahead'], cfg['downsampling_subset'], cfg['step_test'],
            '' if cfg['inversed'] else 'non',
            '' if cfg['look_ahead_only'] else 'non',
            '' if cfg['synthesized_test'] else 'non',
            '' if cfg['filter_only_attack'] else 'non'), test_target)
    np.save('{}_testLab_LB{}_LA{}_DS{}_step{}_{}Inversed_{}LAO'
        '_{}Synth_{}FOA.npy'.format(data_dir, cfg['look_back'],
            cfg['look_ahead'], cfg['downsampling_subset'], cfg['step_test'],
            '' if cfg['inversed'] else 'non',
            '' if cfg['look_ahead_only'] else 'non',
            '' if cfg['synthesized_test'] else 'non',
            '' if cfg['filter_only_attack'] else 'non'), test_label)
    np.save('{}_testLabW_LB{}_LA{}_DS{}_step{}_{}Inversed_{}LAO'
        '_{}Synth_{}FOA.npy'.format(data_dir, cfg['look_back'],
            cfg['look_ahead'], cfg['downsampling_subset'], cfg['step_test'],
            '' if cfg['inversed'] else 'non',
            '' if cfg['look_ahead_only'] else 'non',
            '' if cfg['synthesized_test'] else 'non',
            '' if cfg['filter_only_attack'] else 'non'), test_label_window)

    return input_scaler, output_scaler, train_input, train_target,\
           test_input, test_target, test_label, test_label_window
