from __future__ import division
import os
import numpy as np
import logging
import json
import wandb

from scipy.stats import multivariate_normal
import utils.facilities as facilities

def rescale_window(input_windows, scaler):
    output_windows = []
    for w in input_windows:
        o_w = scaler.inverse_transform(w)
        output_windows.append(o_w)
    output_windows = np.array(output_windows)
    return output_windows

def sMAPE(target, prediction):
    abs_diff = abs(target - prediction)
    amount_err = abs(target + prediction)/2
    #sMAPE = np.sum(abs_diff/amount_err, axis=(1,2))
    sMAPE = np.mean(abs_diff/amount_err)
    return sMAPE



def store_data_model_evaluation(train_input, train_target, test_input,
    test_target, test_label, test_label_window, decoded_train, decoded_test,
    input_scaler, output_scaler, result_dir, cfg, args):

    # Loss evaluation
    rec_loss_train = np.mean((train_target-decoded_train)**2)
    rec_loss_test = np.mean((test_target-decoded_test)**2)
    logging.info("Mean reconstruction loss_train={}, loss_test={}".format(
        rec_loss_train, rec_loss_test))
    #sMAPE: symmetric Mean Absolute Percentage Error
    sMAPE_train = sMAPE(train_target, decoded_train)
    sMAPE_test = sMAPE(test_target, decoded_test)
    logging.info("sMAPE train={}, test={}".format(sMAPE_train, sMAPE_test))

    logging.debug("Before flattening: train_input={},test_input={}".
        format(train_input.shape, test_input.shape))

    # save data just as windows with scaled value
    if args.store_result:
        train_input.dump('{}/swindow_train_input.npk'.format(result_dir))
        train_target.dump('{}/swindow_train_target.npk'.format(result_dir))
        test_input.dump('{}/swindow_test_input.npk'.format(result_dir))
        test_target.dump('{}/swindow_test_target.npk'.format(result_dir))
        decoded_train.dump('{}/swindow_decoded_train.npk'.format(result_dir))
        decoded_test.dump('{}/swindow_decoded_test.npk'.format(result_dir))
        test_label.dump('{}/test_label.npk'.format(result_dir))
        test_label_window.dump('{}/test_label_window.npk'.format(result_dir))

    """ Find threshold: We flatten reconstruction error of training data in
    point-based. Then find the threshold with the assumption that the
    multi-dimensional recon_error follows Gaussian distribution. Then the threshold is
    selected as a 95% percentile of normal recon_error.
    """
    # Flattening
    train_input_flat = facilities.flatten_seq(train_input, cfg['step_train'])
    train_target_flat = facilities.flatten_seq(train_target, cfg['step_train'])
    decoded_train_flat = facilities.flatten_predicted_seq(decoded_train, cfg['step_train'])
    test_input_flat = facilities.flatten_seq(test_input, cfg['step_test'])
    test_target_flat = facilities.flatten_seq(test_target, cfg['step_test'])
    decoded_test_flat = facilities.flatten_predicted_seq(decoded_test, cfg['step_test'])

    logging.debug("After flattening: train_input_flat={},test_input_flat={}\n"
        "train_target_flat={}, test_target_flat={}".format(train_input_flat.shape,
        test_input_flat.shape, train_target_flat.shape, test_target_flat.shape))
    logging.debug("decoded_train={}, decoded_test={}".format(
        decoded_train.shape, decoded_test.shape))

    if args.store_result:
        # save decoded data before rescaling:
        train_input_flat.dump('{}/scaled_train_input.npk'.format(result_dir))
        train_target_flat.dump('{}/scaled_train_target.npk'.format(result_dir))
        decoded_train_flat.dump('{}/scaled_decoded_train.npk'.format(result_dir))
        test_input_flat.dump('{}/scaled_test_input.npk'.format(result_dir))
        test_target_flat.dump('{}/scaled_test_target.npk'.format(result_dir))
        decoded_test_flat.dump('{}/scaled_decoded_test.npk'.format(result_dir))

    # Rescale back to original range before calculating errors
    #train_input_flat = input_scaler.inverse_transform(train_input_flat)
    #test_input_flat = input_scaler.inverse_transform(test_input_flat)
    #test_target_flat = output_scaler.inverse_transform(test_target_flat)
    #train_target_flat = output_scaler.inverse_transform(train_target_flat)
    #decoded_train_flat = output_scaler.inverse_transform(decoded_train_flat)
    #decoded_test_flat = output_scaler.inverse_transform(decoded_test_flat)

    #logging.info("train_target_flat={}, test_target={}, decoded_train={}, "
    #    "decoded_test={}".format(train_target.shape, test_target.shape,
    #    decoded_train.shape, decoded_test.shape))

    # save decoded data after rescaling:
    if args.store_result:
        train_input_flat.dump('{}/train_input.npk'.format(result_dir))
        train_target_flat.dump('{}/train_target.npk'.format(result_dir))
        test_input_flat.dump('{}/test_input.npk'.format(result_dir))
        test_target_flat.dump('{}/test_target.npk'.format(result_dir))
        decoded_train_flat.dump('{}/decoded_train.npk'.format(result_dir))
        decoded_test_flat.dump('{}/decoded_test.npk'.format(result_dir))

    # PART 4: EVALUATION Performance
    error_train_flat = np.abs(decoded_train_flat - train_target_flat) #no_windowsXfeature_out
    error_test_flat = np.abs(decoded_test_flat - test_target_flat)

    logging.debug("error_train_flat={}, error_test_flat={}".format(
        error_train_flat.shape, error_test_flat.shape))

    mu = np.mean(error_train_flat, axis=0)
    sigma = np.cov(error_train_flat, rowvar=False)

    logPD_train = multivariate_normal.logpdf(error_train_flat, mu, sigma)
    # Find the threshold of normal behavior, here we can use minimum/maximum
    # or a 95-percentile of the logPD_train.
    threshold = np.percentile(logPD_train, 0.025)
    logging.info("FLAT: mu={}, sigma={}, threshold={}".format(
        mu, sigma, threshold))

    if args.store_result:
        params = {"mu":mu.tolist(),
                  "sigma":sigma.tolist(),
                  "threshold":threshold}
        with open("{}/mu_sigma_threshold_flat.json".format(
                result_dir), 'w') as json_file:
            json.dump(params, json_file, indent=4)

        logging.info("Store mu, sigma, threshold to file.")
        if not args.test:
            wandb.save("{}/mu_sigma_threshold_flat.json".format(result_dir))

    # EVALUATION WINDOW-based
    # rescale back to original range for windows
    #train_input_w = rescale_window(train_input, input_scaler)
    #train_target_w = rescale_window(train_target, output_scaler)
    #decoded_train_w = rescale_window(decoded_train, output_scaler)
    #test_input_w = rescale_window(test_input, input_scaler)
    #test_target_w = rescale_window(test_target, output_scaler)
    #decoded_test_w = rescale_window(decoded_test, output_scaler)
    #logging.debug("test_target_w={}, decoded_test_w={}".format(
    #    test_target_w.shape, decoded_test_w.shape))

    error_train_w = abs(train_target - decoded_train)
    error_train_w = np.mean(error_train_w, axis=1)

    error_test_w = abs(test_target - decoded_test)
    # error_test_w shape=[number_windows, feature_out]
    error_test_w = np.mean(error_test_w, axis=1)
    logging.info("AVERAGE window-based: error_train={}, error_test={}".format(
        np.mean(error_train_w), np.mean(error_test_w)))

    mu_w = np.mean(error_train_w, axis=0)
    sigma_w = np.cov(error_train_w, rowvar=False)
    logPD_train_w = multivariate_normal.logpdf(error_train_w, mu_w, sigma_w)
    threshold_w = np.percentile(logPD_train_w, 0.025) # should be 0.025
    logging.info("WINDOW: mu={}, sigma={}, threshold={}".format(
        mu_w, sigma_w, threshold_w))
    if args.store_result:
        params = {"mu":mu_w.tolist(),
                  "sigma":sigma_w.tolist(),
                  "threshold":threshold_w}
        with open("{}/mu_sigma_threshold.json".format(result_dir), 'w') as\
                json_file:
            json.dump(params, json_file, indent=4)

        logging.info("Store window-wise mu, sigma, threshold to "
            "{}/mu_sigma_threshold.json.".format(result_dir))
        if not args.test:
            wandb.save("{}/mu_sigma_threshold.json".format(result_dir))


    logPD_test_w = multivariate_normal.logpdf(error_test_w, mu_w, sigma_w)
    pred_test_w = (logPD_test_w < threshold_w) * 1.0

    # LABEL for Windows: convert seq label to a single lable
    test_label_w = np.sum(test_label_window, axis=1) # sum for each sequence/window

    if cfg['synthesized_test']:
        if cfg['synthesized_type'] == 'burst':
            #threshold_count = 0.1*cfg['look_back']
            threshold_count = 0.2 * cfg['synthesized_burst_width']
        else:
            threshold_count = 1
    else:
        threshold_count = 20 # half of second.
    test_label_w = (test_label_w > threshold_count) * 1.

    results = facilities.evaluate_pred_truth(pred_test_w, test_label_w)
    results['loss_train'] = rec_loss_train
    results['loss_test'] = rec_loss_test
    logging.info("Eval results={}".format(results))
    return results
