from __future__ import division

from keras.layers import Input, Dense, Dropout, LeakyReLU
import tensorflow as tf

from keras.layers import Bidirectional, Concatenate
from keras.models import Model
from keras.optimizers import RMSprop, Adam, SGD
from keras.regularizers import l2
import logging


def build_model(use_gpu, cfg):
    """ Construct the model with CPU/GPU LSTM, and load trained weights to this model.
    myLSTM could be CuDNNLSTM or LSTM
    """
    if use_gpu:
        if tf.config.experimental.list_physical_devices('GPU'):
            # Only use CuDNNLSTM for training Cloud model.
            from keras.layers import CuDNNLSTM as myLSTM
        else:
            from keras.layers import LSTM as myLSTM
    else:
        from keras.layers import LSTM as myLSTM

    encoder_layers = cfg['encoder_layers']
    decoder_layers = cfg['decoder_layers']
    n_feature_in = len(cfg['feature_in'])
    n_feature_out = len(cfg['feature_out'])
    loss = cfg['loss']
    learning_rate = cfg['learning_rate']
    droprate = cfg['droprate']
    if cfg['optimizer']=='Adam':
        optimizer = Adam(learning_rate)
    elif cfg['optimizer'] == 'RMSProp':
        optimizer = RMSprop(learning_rate)
    else:
        optimizer = SGD(learning_rate)

    #### ENCODER:
    encoder_inputs = Input(batch_shape=(None, None, n_feature_in))
    logging.info("encoder_inputs={}".format(encoder_inputs.get_shape()))

    max_len = len(cfg['encoder_layers'])
    # single layer:
    return_sequences = True
    if cfg['bidirection']:
        e_outputs, ef_h, ef_c, eb_h, eb_c = Bidirectional(myLSTM(
                            encoder_layers[0],
                            kernel_regularizer=l2(1e-3),
                            return_sequences=return_sequences,
                            return_state=True))(encoder_inputs)
        if cfg['combine_encoder_states'] == 'concat':
            state_h = Concatenate()([ef_h, eb_h])
            state_c = Concatenate()([ef_c, eb_c])
        else:
            state_h = ef_h + eb_h
            state_c = ef_c + eb_c
    else: # UNIDIRECTION
        e_outputs, state_h, state_c = myLSTM(encoder_layers[0],
                            kernel_regularizer=l2(1e-4),
                            return_sequences=return_sequences,
                            return_state=True,
                            stateful=False)(encoder_inputs)

    encoder_states = [state_h, state_c]
    #logging.info("encoder_states[0]={}".format(encoder_states[0].shape))
    e_outputs = Dropout(droprate)(e_outputs)
    e_inputs = e_outputs

    if max_len > 1: # multiple layers
        if cfg['bidirection']:
            for i, layer_dim in enumerate(encoder_layers[1:]):
                e_outputs, ef_h, ef_c, eb_h, eb_c =\
                    Bidirectional(myLSTM(layer_dim,
                            kernel_regularizer=l2(1e-4),
                            return_sequences=return_sequences,
                            return_state=True))(e_inputs)

                if cfg['combine_encoder_states'] == 'concat':
                    state_h = Concatenate()([ef_h, eb_h])
                    state_c = Concatenate()([ef_c, eb_c])
                else:
                    state_h = ef_h + eb_h
                    state_c = ef_c + eb_c
                encoder_states.append(state_h)
                encoder_states.append(state_c)
                if i < max_len - 1:
                    e_outputs = Dropout(droprate)(e_outputs)
                    e_inputs = e_outputs
        else: # unidirection
            # from the second layer we don't need to provide batch_input_shape
            for i, layer_dim in enumerate(encoder_layers[1:]):
                e_outputs, state_h, state_c = myLSTM(layer_dim,
                            kernel_regularizer=l2(1e-4),
                            return_sequences=return_sequences,
                            return_state=True,
                            stateful=False)(e_inputs)
                encoder_states.append(state_h)
                encoder_states.append(state_c)
                if i < max_len - 1:
                    e_outputs = Dropout(droprate)(e_outputs)
                    e_inputs = e_outputs

        logging.info("encoder_states={}".format(encoder_states))

    ### DECODER: set up the decoder, using `encoder_states` as initial state.
    decoder_inputs = Input(shape=(None, n_feature_out))
    # We set up our decoder to return full output sequences,
    # and to return internal states as well. We don't use the
    # return states in the training model, but we will use them in inference.
    initial_states = encoder_states
    d_inputs = decoder_inputs
    decoder_states_inputs = [] # for define Model later
    decoder_lstms = []

    factor = 1
    if cfg['bidirection'] and cfg['combine_encoder_states'] == 'concat':
        factor = 2

    for i, layer_dim in enumerate(decoder_layers):
        decoder_lstm = myLSTM(layer_dim * factor,
                            kernel_regularizer=l2(1e-4),
                            return_sequences=True,
                            return_state=True)
        d_outputs, dhi, dci = decoder_lstm(d_inputs,
            initial_state=initial_states[i*2:i*2+2])
        logging.info("i={}, dhi={}, dci={}".format(i, dhi, dci))
        # input of the higher layer is the output of the previously lower layer

        d_outputs = Dropout(droprate)(d_outputs)
        d_inputs = d_outputs

        # For definition Model later
        d_input_h = Input(shape=(layer_dim*factor, ))
        d_input_c = Input(shape=(layer_dim*factor, ))
        decoder_states_inputs.append(d_input_h)
        decoder_states_inputs.append(d_input_c)
        decoder_lstms.append(decoder_lstm)

    leaky_relu = LeakyReLU(alpha=0.01)
    d_outputs = leaky_relu(d_outputs)

    decoder_dense = Dense(n_feature_out, activation='linear')
    decoder_outputs = decoder_dense(d_outputs)

    # SAVE Model: Define the model that will be saved
    # `encoder_input_data` & `decoder_input_data` into `decoder_target_data`
    model = Model(inputs=[encoder_inputs, decoder_inputs],
                  outputs=decoder_outputs)
    # Encoder Model
    encoder_model = Model(inputs=encoder_inputs, outputs=encoder_states)
    # Decoder Model
    de_inputs = decoder_inputs
    de_states_outputs = []
    for i, layer_dim in enumerate(decoder_layers):
        decoder_lstm = decoder_lstms[i]
        de_states_inputs = decoder_states_inputs[i*2:i*2+2]
        de_outputs, state_h, state_c = decoder_lstm(de_inputs,
                            initial_state=de_states_inputs)
        de_states_outputs += [state_h, state_c]
        de_inputs = de_outputs

    de_outputs = decoder_dense(de_outputs)
    decoder_model = Model(inputs=[decoder_inputs] + decoder_states_inputs,
                           outputs=[de_outputs] + de_states_outputs)
    # Run training
    model.compile(optimizer = optimizer, loss = loss, metrics = ['accuracy'])
    logging.info(model.summary())

    return model, encoder_model, decoder_model


#class LstmSeq2Seq(object):
#    """ LstmSeq2Seq is to encode an input sequence into a encoded_states=[state_h, state_c].
#    Then the first symbol is decoded based on the START token and encoded_states, and then
#    subsequent symbol is decoded based on the previous symbol and the previous states.
#    Here the training phase is different from inference phase, we build two
#    different decoder for the two purposes.
#    """
#    def __init__(self, START, cfg):
#        self.window = cfg['look_back']
#        self.n_feature_out = len(cfg['feature_out'])
#        self.START = START
#        self.cfg = cfg
#
#    def build_model(self):
#        self.model, self.encoder_model, self.decoder_model =\
#            build_models(self.cfg['use_gpu'], self.cfg)
#        return self.model, self.encoder_model, self.decoder_model
#
#    def encoded_state(self, input_seq):
#        encoded_states = self.encoder_model.predict(input_seq)
#        return encoded_states
#
#    def decode_sequence(self, input_seq):
#        # Encode the input as state vectors.
#        START = 0
#        STOP = np.full((1, self.n_feature_out), 100)
#        batch_size = input_seq.shape[0]
#        target_seq = np.full((batch_size, 1, self.n_feature_out), START)
#        encoded_states = self.encoder_model.predict(input_seq,
#            batch_size=batch_size)
#        # encoded_states = [h1,c1, h2, c2, h3,c3,...] are couple of each layer.
#        decoder_states = encoded_states
#        decoded_seq = None
#        stop_condition = False
#        while not stop_condition:
#            outs  = self.decoder_model.predict([target_seq] + decoder_states)
#            output_value = outs[0]
#            decoder_states = outs[1:]
#            # Exit condition: either hit max length or find stop character.
#            #logging.debug("decoded_seq.shape={}".format(decoded_seq.shape))
#            if (output_value == STOP).all():
#                break # no need to concatenate STOP to output sequence
#
#            if decoded_seq is None:
#                decoded_seq = output_value
#            else:
#                decoded_seq = np.concatenate((decoded_seq, output_value), axis=1)
#            if decoded_seq.shape[1] > self.window - 1:
#                stop_condition = True
#            # Update the target sequence (of length 1).
#            target_seq = output_value
#
#        return decoded_seq
