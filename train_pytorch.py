
""" Author: Mao Ngo
Created data: 2020-03-17
"""
from __future__ import division

import os
import numpy as np
import logging
import time
import importlib
import argparse
import matplotlib
matplotlib.use("Agg")

import torch
import torch.optim as optim
import torch.nn as nn
import wandb
from sklearn.model_selection import TimeSeriesSplit
from sklearn.model_selection import KFold
import evaluation as eval
from torchsummary import summary

def sizeof_fmt(num, suffix='B'):
    # thanks: https://stackoverflow.com/questions/1094841/reusable-library-to-get-human-readable-version-of-file-size
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

def log_model_size(model, cfg):
    logging.info(model)
    window_size = int(cfg['look_back']/cfg['downsampling_subset'])
    input_size = (window_size, len(cfg['feature_in']))
    s_model = summary(model, input_size=input_size)
    logging.info(s_model)
    bits = 32 # 32 bit floating-point number
    params = s_model.trainable_params*bits
    logging.info("Size of trainable params={}".format(sizeof_fmt(params)))

def train(e_input, target, seq2seq, optimizer, criterion, teacher_forcing):

    seq_len = e_input.size(1)
    optimizer.zero_grad()
    output = seq2seq(e_input, target, teacher_forcing, True)
    #logging.info("output={}".format(output.shape))
    loss = criterion(output, target)
    loss.backward()
    optimizer.step()

    return loss.item()

def batch_data(data, batch_size, shuffle=False):
    if shuffle:
        np.random.seed(100)
        np.random.shuffle(data)

    batch_data = list()
    for i in range(0, len(data), batch_size):
        if i + batch_size < len(data):
            b_ = data[i: i+batch_size]
        else:
            # Filling the last batch by repeating itself in the round-robin.
            b_temp = data[i:]
            b_ = b_temp
            j = 0
            while len(b_) < batch_size:
                b_ = np.concatenate((b_, b_temp[j:j+1]), axis=0)
                j +=1
                if j == len(b_temp):
                    j = 0
        b_ = torch.from_numpy(np.asarray(b_)).float()
        batch_data.append(b_)
    return batch_data

def unison_shuffled_copies(a, b):
    # thanks: https://stackoverflow.com/questions/4601373/better-way-to-shuffle-two-numpy-arrays-in-unison
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]

def train_iterations(train_input, target_output, valid_in, valid_target,
        seq2seq, device, cross_i, n_epochs, store_result, checkpoint_path, cfg):

    batch_size = cfg['batch_size']
    learning_rate = cfg['learning_rate']
    teacher_forcing_ratio = cfg['teacher_forcing_ratio']
    decay = teacher_forcing_ratio / float(n_epochs)
    # manually decay teacher_forcing_ratio:
    teacher_forcing_ratio *= (1./ (1. + decay*(1.+cross_i)))

    seq2seq.to(device=device)

    validation = cfg['validation']
    if cfg['loss'] == 'mean_squared_error':
        criterion = nn.MSELoss()
    elif cfg['loss'] == 'mean_absolute_error':
        criterion = nn.L1Loss()

    print_every = 20
    plot_losses = []
    print_loss_total = 0 # Reset every print_every
    plot_loss_total = 0 # Reset every print_every

    if cfg['optimizer'] == 'SGD':
        optimizer = optim.SGD(seq2seq.parameters(), lr=learning_rate)
    elif cfg['optimizer'] == 'Adam':
        optimizer = optim.Adam(seq2seq.parameters(), lr=learning_rate)
    elif cfg['optimizer'] == 'RMSprop':
        optimizer = optim.RMSprop(seq2seq.parameters(), lr=learning_rate)

    best_valid_loss = 0
    for epoch in range(n_epochs):
        b_inputs = batch_data(train_input, batch_size)
        b_target_outputs = batch_data(target_output, batch_size)
        seq2seq.train()

        for bi, b_input in enumerate(b_inputs):
            input_tensor = b_input.to(device=seq2seq.device)
            target_tensor = b_target_outputs[bi].to(device=seq2seq.device)

            #loss = train(input_tensor, target_tensor,
            #    seq2seq, optimizer, criterion, teacher_forcing_ratio)
            # TRAINING
            optimizer.zero_grad()
            output = seq2seq(input_tensor, target_tensor, teacher_forcing_ratio, True)
            #logging.info("output={}".format(output.shape))
            loss = criterion(output, target_tensor)
            loss.backward()
            optimizer.step()
            loss = loss.item()

            print_loss_total += loss
            plot_loss_total += loss
            if bi % print_every == 0:
                print_loss_avg = print_loss_total/print_every
                print_loss_total = 0
                logging.info("epoch={}, batch={} loss={}".format(epoch, bi, loss))
                plot_losses.append(print_loss_avg)
            if store_result:
                wandb.log({'train_loss epoch_{}'.format(cross_i):loss})

        train_input, target_output = unison_shuffled_copies(train_input,
            target_output)

        #### VALIDATION
        if epoch % cfg['freq_val'] == 0 and cfg['early_stopping']:
            with torch.no_grad():
                valid_pred = decode_seq_data(valid_in, seq2seq, cfg)
                if cfg['loss'] == 'mean_square_error':
                    valid_loss = np.mean((valid_target - valid_pred)**2)
                elif cfg['loss'] == 'mean_absolute_error':
                    valid_loss = np.mean(abs(valid_target - valid_pred))

            if store_result:
                wandb.log({'valid_loss over iteration':valid_loss})
            logging.info('Cross={} Epoch={} valid_loss={}'.format(
                cross_i, epoch+1, valid_loss))

            if valid_loss < best_valid_loss or epoch == cfg['freq_val']\
                    or epoch == 0:
                best_valid_loss = valid_loss
                checkpoint = {'epoch':epoch+1,
                              'best_valid_loss':best_valid_loss,
                              'state_dict':seq2seq.state_dict(),
                              'optimizer':optimizer.state_dict()
                             }
                torch.save(checkpoint, checkpoint_path)
                nb_without_improvements = 0
                logging.info("Cross={}, Epoch={}, Best valid loss={}".format(
                    cross_i, epoch+1, best_valid_loss))
            else:
                nb_without_improvements += cfg['freq_val']

            if nb_without_improvements > cfg['early_stop_patience']:
                logging.info("Early stop cross validation {} at epoch {}"
                    " with weights from epochs {}".
                    format(cross_i, epoch, epoch - nb_without_improvements))
                break

    checkpoint = torch.load(checkpoint_path)
    seq2seq.load_state_dict(checkpoint['state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer'])

    return plot_losses


def train_and_test(args, cfg):
    data = importlib.import_module('dataset.turtlebot3')

    # PART 1: Preprocess data
    input_scaler, output_scaler, train_input, train_target, test_input,\
        test_target, test_label, test_label_window = data.get_data(cfg)

    logging.info("train_input={}, train_target={}".format(train_input.shape,
        train_target.shape))
    logging.info("test_input={}, test_target={}".format(test_input.shape,
        test_target.shape))

    if cfg['use_gpu']:
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    else:
        device = torch.device("cpu")

    # PART 2: Build and Train the GRU model
    if args.model == 'gru_seq2seq':
        import model.gru_seq2seq_pytorch as seq2seq_pytorch
    else:
        import model.lstm_seq2seq_pytorch as seq2seq_pytorch
    if args.attention:
        seq2seq = seq2seq_pytorch.AttentionSeq2Seq(cfg, device)
    else:
        seq2seq = seq2seq_pytorch.Seq2Seq(cfg, device)
    log_model_size(seq2seq, cfg)

    result_dir = 'results/{}/{}/{}_pytorch{}'.format(args.dataset,
        args.scenario, args.model, '_attention' if args.attention else '')
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)

    model_dir ='trained_model/{}/{}/{}{}'.format(args.dataset, args.scenario,
        args.model, '_attention' if args.attention else '')
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    model_path = '{}/{}_seq2seq.pth'.format(model_dir, args.model)
    checkpoint_path = '{}/checkpoint.pth'.format(model_dir)

    ## TRAIN the model
    n_splits = 5
    if cfg['cross_validation']:
        logging.info("n_splits={}".format(n_splits))
        kfold = KFold(n_splits=n_splits, shuffle=True, random_state=0)
        cross_i = 0
        for train_id, valid_id in kfold.split(train_input):
            # ignore some sample for fitting batch_size
            logging.info("Cross_i={}, LENGTH train_id={}, valid_id={}".format(
                cross_i, len(train_id), len(valid_id)))
            logging.info("After LENGTH train_id={}, valid_id={}".format(
                len(train_id), len(valid_id)))
            train_in, valid_in = train_input[train_id], train_input[valid_id]
            decoder_target, valid_target = train_target[train_id], train_target[valid_id]
            logging.info("train_in={}, valid_in={}".format(train_in.shape, valid_in.shape))

            plot_losses = train_iterations(train_in, decoder_target,
                valid_in, valid_target, seq2seq, device, cross_i, args.nb_epochs,
                args.store_result, checkpoint_path, cfg)
            cross_i += 1
    else:
        i = 0
        #for i in range(n_splits):
        plot_losses = train_iterations(train_input, train_target,
            test_input, test_target, seq2seq, device, i, args.nb_epochs,
            args.store_result, checkpoint_path, cfg)

    if args.store_result:
        logging.info('Save seq2seq model at: {}'.format(model_path))
        torch.save(seq2seq.state_dict(), model_path)
        wandb.save(model_path)


    # PART 3: PREDICTING/TESTING
    with torch.no_grad():
        decoded_train = decode_seq_data(train_input, seq2seq, cfg)
        decoded_test = decode_seq_data(test_input, seq2seq, cfg)

    logging.info("decoded_train={}, decoded_test={}".format(
        decoded_train.shape, decoded_test.shape))

    eval.store_data_model_evaluation(train_input, train_target, test_input,
        test_target, test_label, test_label_window, decoded_train, decoded_test,
        input_scaler, output_scaler, result_dir, cfg, args)


def decode_seq_data(X, seq2seq, cfg):
    decoded_data = None
    batch_size = cfg['batch_size_inference']

    logging.info("Decode total mini-batch={}/{}={}".format(len(X), batch_size,
        int(len(X)/batch_size)))

    seq2seq.eval()
    seq2seq.to(seq2seq.device)
    freq_print = 10

    latencies = []
    i = 0
    while i < len(X):
        start_batch = time.time()
        if i + batch_size > len(X):
            last_batch_len = len(X) - i
            batch_i = np.concatenate((X[i:],
                np.full((batch_size-last_batch_len, X.shape[1], X.shape[2]),0)),
                axis=0)
        else:
            batch_i = X[i : i + batch_size]

        batch_i = torch.from_numpy(batch_i).float().to(seq2seq.device)
        decoded_seq = seq2seq(batch_i).detach().cpu().numpy()

        if i + batch_size > len(X):
            decoded_seq = decoded_seq[:last_batch_len]

        if decoded_data is None:
            decoded_data = decoded_seq
        else:
            decoded_data = np.concatenate((decoded_data, decoded_seq), axis=0)
        latency_batch = time.time() - start_batch
        latencies.append(latency_batch)
        if (i + 1) % freq_print == 0:
            logging.debug("start i={}, batch_i={}, latency={}".format(i,
                batch_i.shape, latency_batch))
        i += batch_size
    #decoded_data = np.asarray(decoded_data)
    logging.info("Average latency={}, batch_size={}, decoded_data={}".format(
        np.mean(latencies), batch_size, decoded_data.shape))
    return decoded_data

def test_seq2seq(args, cfg):
    logging.info("Testing model {} for {}".format(args.model, args.dataset))
    # PART 1: Prepare data
    data = importlib.import_module('dataset.{}'.format(args.dataset))
    input_scaler, output_scaler, train_input, train_target, test_input,\
        test_target, test_label, test_label_window = data.get_data(cfg)

    # PART 2: load the trained model
    if args.model == 'gru_seq2seq':
        import model.gru_seq2seq_pytorch as seq2seq_pytorch
    else:
        import model.lstm_seq2seq_pytorch as seq2seq_pytorch
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    if args.attention:
        seq2seq = seq2seq_pytorch.AttentionSeq2Seq(cfg, device)
    else:
        seq2seq = seq2seq_pytorch.Seq2Seq(cfg, device)

    result_dir = 'results/{}/{}/{}_pytorch{}'.format(args.dataset,
        args.scenario, args.model, '_attention' if args.attention else '')
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)

    if args.model_path is not None:
        model_path = args.model_path
    else:
        model_dir ='trained_model/{}/{}/{}{}'.format(args.dataset, args.scenario,
            args.model, '_attention' if args.attention else '')
        model_path = "{}/{}_seq2seq.pth".format(model_dir, args.model)
    seq2seq.load_state_dict(torch.load(model_path, map_location=seq2seq.device))
    seq2seq.eval() # ensure dropout is ignored
    seq2seq.to(seq2seq.device)
    logging.info("Loaded seq2seq model from file={}.".format(model_path))
    log_model_size(seq2seq, cfg)

    # PART 3: prediction
    with torch.no_grad():
        decoded_train = decode_seq_data(train_input, seq2seq, cfg)
        decoded_test = decode_seq_data(test_input, seq2seq, cfg)

    logging.info("decoded_train={}, decoded_test={}".format(
        decoded_train.shape, decoded_test.shape))
    eval.store_data_model_evaluation(train_input, train_target, test_input,
        test_target, test_label, test_label_window, decoded_train, decoded_test,
        input_scaler, output_scaler, result_dir, cfg, args)

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--verbose',
        action='store_true')
    parser.add_argument(
        '--test',
        action='store_true',
        help='Only test the trained model (stored in trained_model folder).')
    parser.add_argument(
        '--dataset',
        type=str,
        help='Experiment dataset: turtlebot3 (Default), kobuki.',
        default='turtlebot3')
    parser.add_argument(
        '--scenario',
        type=str,
        help='Experiment scenario: pentagon (default), triangle, zigzag, square (for kobuki).',
        default='pentagon')
    parser.add_argument(
        '--nb_epochs',
        help='Number of epochs for training. Default is 2.',
        type=int,
        default=2)
    parser.add_argument(
        '--model',
        type=str,
        help='Model name: lstm_seq2seq, and gru_seq2seq (default).',
        default='gru_seq2seq')
    parser.add_argument(
        '--model_path',
        type=str,
        help='Model path for loading and inference only. Example: trained_model/fedml_robot/gru_seq2seq.pth',
        default=None)
    parser.add_argument(
        '--plot',
        action='store_true',
        help='Enable plot graph if running with Xserver.')
    parser.add_argument(
        '--store_result',
        action='store_true',
        help='Enable to store inputs and reconstructions of train and test sets.')
    parser.add_argument('--attention',
        action='store_true',
        help='Enable to attention decoder mode.')

    args = parser.parse_args()
    mode = 'testing' if args.test else 'training'

    cfg_path = 'config.turtlebot3_config_pytorch{}'.format(
        '_attention' if args.attention else '')
    cfg = importlib.import_module(cfg_path).config
    cfg['robot'] = args.dataset
    cfg['scenario']=args.scenario

    log_dir = 'logfiles/{}/{}'.format(args.dataset, args.scenario)
    FILENAME = '{}/{}_{}_pytorch{}.log'.format(log_dir, mode, args.model,
        '_attention' if args.attention else '')
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    if args.verbose:
        LOG_LVL = logging.DEBUG
    else:
        LOG_LVL = logging.INFO
    FORMAT = '%(asctime)-15s %(levelname)s %(filename)s %(lineno)s:: %(message)s'

    logging.getLogger('matplotlib.font_manager').disabled = True

    fileHandler = logging.FileHandler(FILENAME, mode='w')
    fileHandler.setFormatter(logging.Formatter(FORMAT))

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logging.Formatter(FORMAT))

    logger = logging.getLogger('')
    if logger.hasHandlers():
        logger.handlers.clear()
    logger.addHandler(consoleHandler)
    logger.addHandler(fileHandler)
    logger.setLevel(LOG_LVL)

    logging.info(cfg)

    if args.test:
        test_seq2seq(args, cfg)
    else:
        if args.store_result:
            model_name = '{}{}'.format(
                'Bi' if cfg['bidirection_encoder'] else '',
                'GRU' if args.model == 'gru_seq2seq' else 'LSTM')
            wandb.init(
                project='RobotAanomalyDetectionPytorch',
                name='{}{}_{}{}_{}L{}'.format(args.dataset, args.scenario,
                model_name, '_Att' if args.attention else '',
                cfg['num_layers'], cfg['hidden_size']),
                config={'cfg':cfg})
            wandb.config.update(args)

        logging.info("run train_and_test()")
        train_and_test(args, cfg)
