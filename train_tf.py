""" Author: Mao Ngo
Created data: 2020-03-17
"""
from __future__ import division

import os
#os.environ["CUDA_VISIBLE_DEVICES"]="0"
import numpy as np
import logging
import time
import importlib
import argparse
import matplotlib
matplotlib.use("Agg")

import tensorflow as tf
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint
from keras.models import load_model
from keras.utils.vis_utils import plot_model
import wandb
from wandb.keras import WandbCallback
from sklearn.model_selection import TimeSeriesSplit
from sklearn.model_selection import KFold
import evaluation as eval

def get_flops(model):
    run_meta = tf.compat.v1.RunMetadata()
    opts = tf.compat.v1.profiler.ProfileOptionBuilder.float_operation()
    # We use the Keras session graph in the call to the profiler.
    flops = tf.compat.v1.profiler.profile(
        graph=tf.compat.v1.keras.backend.get_session().graph,
        run_meta=run_meta, cmd='op', options=opts)
    return flops.total_float_ops  # Prints the "flops" of the model.


def train_and_test(args, cfg):
    data = importlib.import_module('dataset.{}'.format(args.dataset))

    window_size = int(cfg['look_back']/cfg['downsampling_subset'])
    feature_out = cfg['feature_out']

    # PART 1: Preprocess data
    input_scaler, output_scaler, train_input, train_target, test_input,\
        test_target, test_labels, test_label_window = data.get_data(cfg)

    START = np.full((1, len(feature_out)), 0)
    # padding START to the begin of each decoder_input window,
    decoder_input = []
    for i, d_i in enumerate(train_target):
        # append a special token START at the beginning
        n_d_i = np.vstack((START, d_i[:-1,:]))
        decoder_input.append(n_d_i)

    decoder_input = np.asarray(decoder_input)

    logging.info("train_input={}, train_target={}, decoder_input={}".format(
        train_input.shape, train_target.shape,
        decoder_input.shape))
    logging.info("test_input={}, test_target={}".format(test_input.shape,
        test_target.shape))

    # PART 2: Build and Train the LSTM model
    result_dir = 'results/{}/{}/{}'.format(args.dataset, cfg['scenario'],
        args.model)
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)
    model_dir ='trained_model/{}/{}/{}'.format(args.dataset, cfg['scenario'],
        args.model)
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    best_model = '{}/{}.h5'.format(model_dir, args.model)

    if args.model == 'lstm_seq2seq':
        import model.lstm_seq2seq as base_model
    else:
        import model.gru_seq2seq as base_model
    model, encoder, decoder = base_model.build_model(cfg['use_gpu'], cfg)
    logging.info("Total flops={}".format(get_flops(model)))
    plot_model(model, to_file='{}/model.png'.format(model_dir), show_shapes=True)

    ## TRAIN the model
    # Callback list
    es = EarlyStopping(monitor='val_loss', mode='min', verbose=1,
        patience=cfg['early_stop_patience']) #, restore_best_weights=True)
    mc = ModelCheckpoint(best_model, monitor='val_loss', mode='min',verbose=1)
    if args.store_result:
        cb_list = [WandbCallback(), es, mc]
    else:
        cb_list = [es, mc]

    n_splits = 5
    #tscv = TimeSeriesSplit(n_splits=n_splits)
    kfold = KFold(n_splits=n_splits, shuffle=True, random_state=0)
    cross_i = 0
    for train_id, valid_id in kfold.split(train_input):
        # ignore some sample for fitting batch_size
        logging.info("Cross_i={}, LENGTH train_id={}, valid_id={}".format(
            cross_i, len(train_id), len(valid_id)))
        train_id = train_id[:int(len(train_id)/cfg['batch_size'])*cfg['batch_size']]
        valid_id = valid_id[:int(len(valid_id)/cfg['batch_size'])*cfg['batch_size']]
        logging.info("After LENGTH train_id={}, valid_id={}".format(
            len(train_id), len(valid_id)))

        train_in, valid_in = train_input[train_id], train_input[valid_id]
        dec_in,val_dec_in = decoder_input[train_id], decoder_input[valid_id]
        dec_ou,val_dec_ou = train_target[train_id], train_target[valid_id]

        logging.info("train_in={}, valid_in={}".format(train_in.shape,
            valid_in.shape))

        hist = model.fit([train_in, dec_in], dec_ou,
                batch_size=cfg['batch_size'],
                epochs=args.nb_epochs,
                shuffle=True,
                validation_data=([valid_in, val_dec_in], val_dec_ou),
                verbose=args.verbose*1,
                callbacks=cb_list)
        logging.info("Avg train loss={}, Avg val loss={}".format(
                np.mean(hist.history['loss']),
                np.mean(hist.history['val_loss'])))
        # Load the best model for training in the next cross-validation set:
        model = load_model(best_model)
        cross_i += 1

    # PART 3: PREDICTING/TESTING
    decoded_train = decode_seq_data(train_input, encoder, decoder, cfg)
    decoded_test = decode_seq_data(test_input, encoder, decoder, cfg)
    logging.info("decoded_train={}, decoded_test={}".format(
        decoded_train.shape, decoded_test.shape))
    assert decoded_test.shape[1] == window_size,\
        'Wrong decoded shape {}'.format(decoded_test.shape)

    eval.store_data_model_evaluation(train_input, train_target, test_input,
        test_target, test_labels, test_label_window, decoded_train, decoded_test,
        input_scaler, output_scaler, result_dir, cfg, args)

    if args.store_result:
        model.save_weights('{}/{}.weights'.format(model_dir, args.model))
        logging.info('Save encoder: {}/{}_encoder.h5'.format(model_dir, args.model))
        encoder.save('{}/{}_encoder.h5'.format(model_dir, args.model))
        encoder.save_weights('{}/{}_encoder.weights'.format(model_dir, args.model))
        logging.info('Save decoder: {}/{}_decoder.h5'.format(model_dir, args.model))
        decoder.save('{}/{}_decoder.h5'.format(model_dir, args.model))
        decoder.save_weights('{}/{}_decoder.weights'.format(model_dir, args.model))

def decode_seq_data(X, encoder, decoder, cfg):
    decoded_data = None
    batch_size = cfg['batch_size_inference']
    window_size = int(cfg['look_back']/cfg['downsampling_subset'])

    START = np.full((batch_size, 1, len(cfg['feature_out'])), 0)
    logging.info("total mini-batch={}/{}={}".format(len(X), batch_size,
        int(len(X)/batch_size)))
    latencies = []
    i = 0
    while i < len(X):
        start_batch = time.time()
        if i + batch_size > len(X):
            last_batch_len = len(X) - i
            batch_i = np.concatenate((X[i:],
                np.full((batch_size - last_batch_len, X.shape[1], X.shape[2]),0)),
                axis=0)
        else:
            batch_i = X[i : i + batch_size]

        encoded_states = encoder.predict(batch_i, batch_size=batch_size)
        target_seq = START
        # np.full((batch_size, 1, len(cfg['feature_out'])), START)
        # Quick-fix for GRU-Keras with Single layer GRUs:
        if type(encoded_states).__module__ == np.__name__:
            # Single layer, just one numpy array
            decoder_states = [encoded_states]
        else:
            decoder_states = encoded_states
        decoded_seq = None
        stop_condition = False
        while not stop_condition:
            outs = decoder.predict([target_seq] + decoder_states)
            output_value = outs[0]
            decoder_states = outs[1:]
            if decoded_seq is None:
                decoded_seq = output_value
            else:
                decoded_seq = np.concatenate((decoded_seq, output_value), axis=1)
            if decoded_seq.shape[1] > window_size - 1:
                stop_condition = True
            # Update the target sequence (of length 1).
            target_seq = output_value

        if i + batch_size > len(X):
            decoded_seq = decoded_seq[:last_batch_len]

        if decoded_data is None:
            decoded_data = decoded_seq
        else:
            decoded_data = np.concatenate((decoded_data, decoded_seq), axis=0)
        latency_batch = time.time() - start_batch
        latencies.append(latency_batch)
        #logging.debug("start i={}, batch_i={}, latency={}".format(i,
        #    batch_i.shape, latency_batch))
        i += batch_size
    logging.info("Average latency={}, batch_size={}, decoded_data={}".format(
        np.mean(latencies), batch_size, decoded_data.shape))
    return decoded_data

def test_seq2seq(args, cfg):
    if args.model == 'lstm_seq2seq':
        import model.lstm_seq2seq as base_model
    else:
        import model.gru_seq2seq as base_model

    logging.info("Testing model {} for {}".format(args.model, args.dataset))

    # PART 1: Prepare data
    data = importlib.import_module('dataset.{}'.format(args.dataset))
    input_scaler, output_scaler, train_input, train_target, test_input,\
        test_target, test_labels, test_label_window = data.get_data(cfg)

    # PART 2: load the trained model
    """ This code is used if we load the trained model from the same training platform.
    In other words, if we trained with CuDNNLSTM, we load and inference with CuDNNLSTM is OK.
    However, if we want to use CuDNNLSTM to accelerate training, then we load saved weights
    to the CPU LSTM layer for inference, we have to do as the next snipet code.
    """
    result_dir = 'results/{}/{}/{}'.format(args.dataset, cfg['scenario'],
        args.model)
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)
    model_dir ='trained_model/{}/{}/{}'.format(args.dataset, cfg['scenario'],
        args.model)
    if args.test_with_gpu:
        logging.info("Test with GPU CuDNNLSTM layers.")
        encoder = load_model("{}/{}_encoder.h5".format(
            model_dir, args.model))
        decoder = load_model("{}/{}_decoder.h5".format(
            model_dir, args.model))
    else: ## For CPU inference:
        logging.info("Test with CPU LSTM layers.")
        use_gpu=False
        model, encoder, decoder = base_model.build_model(use_gpu, cfg)
        encoder.load_weights("{}/{}_encoder.weights".format(
            model_dir, args.model))
        decoder.load_weights("{}/{}_decoder.weights".format(
            model_dir, args.model))

    # PART 3: prediction
    decoded_train = decode_seq_data(train_input, encoder, decoder, cfg)
    decoded_test = decode_seq_data(test_input, encoder, decoder, cfg)
    logging.info("decoded_train={}, decoded_test={}".format(
        decoded_train.shape, decoded_test.shape))
    eval.store_data_model_evaluation(train_input, train_target, test_input,
        test_target, test_labels, test_label_window, decoded_train, decoded_test,
        input_scaler, output_scaler, result_dir, cfg, args)

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--verbose',
        action='store_true')
    parser.add_argument(
        '--test',
        action='store_true')
    parser.add_argument(
        '--test_with_gpu',
        action='store_true',
        help="Use GPU device (if available) to load and test the trained model.")
    parser.add_argument(
        '--dataset',
        type=str,
        help='Experiment dataset: turtlebot3. Default is turtlebot3.',
        default='turtlebot3')
    parser.add_argument(
        '--nb_epochs',
        help='Number of epochs for training. Default is 2.',
        type=int,
        default=2)
    parser.add_argument(
        '--model',
        type=str,
        help='Model name: lstm_seq2seq (default), and gru_seq2seq.',
        default='lstm_seq2seq')
    parser.add_argument(
        '--plot',
        action='store_true',
        help='Enable plot graph if running with Xserver.')
    parser.add_argument(
        '--store_result',
        action='store_true',
        help='Enable to store input and reconstructed results of valid'
            ' and test sets.')

    args = parser.parse_args()
    mode = 'testing' if args.test else 'training'
    cfg = importlib.import_module('config.{}_config'.format(
        args.dataset)).config

    log_dir = 'logfiles/{}/{}'.format(args.dataset, cfg['scenario'])
    FILENAME = '{}/{}_{}_tf.log'.format(log_dir, mode, args.model)
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    if args.verbose:
        LOG_LVL = logging.DEBUG
    else:
        LOG_LVL = logging.INFO
    FORMAT = '%(asctime)-15s %(levelname)s %(filename)s %(lineno)s:: %(message)s'

    logging.getLogger('matplotlib.font_manager').disabled = True

    fileHandler = logging.FileHandler(FILENAME, mode='w')
    fileHandler.setFormatter(logging.Formatter(FORMAT))

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logging.Formatter(FORMAT))

    logger = logging.getLogger('')
    if logger.hasHandlers():
        logger.handlers.clear()
    logger.addHandler(consoleHandler)
    logger.addHandler(fileHandler)
    logger.setLevel(LOG_LVL)

    logging.info(cfg)

    if args.test:
        test_seq2seq(args, cfg)
    else:
        if args.store_result:
            wandb.init(
                project='RobotAanomalyDetectionTF',
                name='{}_{}_{}'.format(args.dataset, cfg['scenario'], args.model),
                config={'args':args,'cfg':cfg}
                )
            wandb.config.update(args)

        logging.info("run train_and_test()")
        train_and_test(args, cfg)
